import eel
from pytube import YouTube

eel.init('web')

@eel.expose
def dl_yt_video(url):
    #YouTube(url).streams.first().download()
    #print(YouTube(url).thumbnail_url)
    yt_thumbnail = YouTube(url).thumbnail_url
    yt_title = YouTube(url).title
    yt_description = YouTube(url).description[0:200] + " ..."
    yt_streams = YouTube(url).fmt_streams
    #eel.thumbnail_insert()("HALLO")
    #return("HALLO")
    eel.create_yt_card(yt_title, yt_description, yt_thumbnail, yt_streams)

    #eel.thumbnail_insert()(lambda n: print (YouTube(url).thumbnail_url))


eel.start('index.html', mode='default')
