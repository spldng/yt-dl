

//  ########## function generates a card with data of youtube video ##########
eel.expose(create_yt_card);
function create_yt_card(yt_title, yt_description, yt_thumbnail_path, yt_streams)  {
  console.log(yt_thumbnail_path)
  document.getElementById("yt-result").innerHTML += "<div class='uk-card uk-card-default uk-dark uk-grid-collapse uk-child-width-1-2@s uk-margin' uk-grid>"
                                                      +"<div class='uk-card-media-left uk-cover-container'>"
                                                        +"<img src='"+yt_thumbnail_path+"' alt='' uk-cover>"
                                                        +"<canvas width='600' height='400'></canvas>"
                                                      +"</div>"
                                                      +"<div>"
                                                        +"<div class='uk-card-body'>"
                                                          +"<h4 class='uk-card-title'>"+yt_title+"</h4>"
                                                          +"<p class='uk-text-small'>"+yt_description+"</p>"


                                                          +"<form>"
                                                            +"<select class='uk-select>"
                                                              +"<optgroup label='Video & Audio'>"
                                                                +"<option>1080</option>"
                                                                +"<option>720</option>"
                                                              +"</optgroup>"
                                                              +"<optgroup label='Only Audio'>"
                                                                +"<option>1080</option>"
                                                                +"<option>720</option>"
                                                              +"</optgroup>"
                                                            +"</select>"
                                                          +"</form>"

                                                        +"</div>"
                                                      +"</div>"
                                                    +"</div>";
}


document.getElementById("yt-dl-btn").addEventListener("click", function(){
  var yt_url = document.getElementById("yt-dl-url").value;
  console.log(yt_url)
  //yt_thumbnail_path = eel.dl_yt_video(yt_url)
  //console.log(eel.dl_yt_video(yt_url))
  eel.dl_yt_video(yt_url)


  //eel.dl_yt_video(yt_url) (function(yt_thumbnail_path) {
  //  console.log(yt_thumbnail_path)

//  });

});
